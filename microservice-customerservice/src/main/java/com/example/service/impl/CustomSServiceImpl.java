package com.example.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.constant.ConversationEnum;
import com.example.constant.CustomSEnum;
import com.example.pojo.dto.ConversationDTO;
import com.example.pojo.dto.CustomSDTO;
import com.example.pojo.vo.ClientMessageVo;
import com.example.pojo.vo.MessageVo;
import com.example.service.CustomSService;
import com.example.tools.RedisTools;
import com.example.tools.SortListTools;
import com.example.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
@Slf4j
@Service
public class CustomSServiceImpl implements CustomSService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private WebSocketServer webSocket;

    @Override
    public void sendToCs(ClientMessageVo clientMessageVo) throws IOException {
        log.info(clientMessageVo.getMessage());
        //获取聊天会话
        boolean flag = RedisTools.existsValue(stringRedisTemplate, ConversationEnum.CONVERSATION_LIST.getKey() + clientMessageVo.getUserId());
        if (!flag) {
            //分配新的客服
            List<CustomSDTO> lists = RedisTools.hget(stringRedisTemplate, CustomSEnum.CUSTOM_S_LIST.getKey(), CustomSDTO.class);
            List<CustomSDTO> customLists = (List<CustomSDTO>) SortListTools.sort(lists, "number", SortListTools.ASC);
            CustomSDTO customSDTO = customLists.get(0);
            //将消息发送给人数最小的客服
            MessageVo messageVo = new MessageVo();
            messageVo.setMessage(clientMessageVo.getMessage());
            messageVo.setClient(clientMessageVo.getClient());
            messageVo.setUserId(clientMessageVo.getUserId());
            messageVo.setServer(customSDTO.getServer());
            webSocket.sendMessageToServer(messageVo);

            //新增用户会话
            ConversationDTO conversationDTO = new ConversationDTO();
            conversationDTO.setServer(customSDTO.getServer());
            conversationDTO.setUserId(clientMessageVo.getUserId());
            RedisTools.set(stringRedisTemplate, ConversationEnum.CONVERSATION_LIST.getKey() + clientMessageVo.getUserId(), JSONObject.toJSONString(conversationDTO));
            //新增客服服务人数
            customSDTO.setNumber(customSDTO.getNumber() + 1);
            RedisTools.hset(stringRedisTemplate, CustomSEnum.CUSTOM_S_LIST.getKey(), customLists.get(0).getServer(), JSONObject.toJSONString(customSDTO));
            return;
        }
        //获取会话
        ConversationDTO conversationDTO = RedisTools.get(stringRedisTemplate, ConversationEnum.CONVERSATION_LIST.getKey() + clientMessageVo.getUserId(), ConversationDTO.class);
        MessageVo messageVo = new MessageVo();
        messageVo.setMessage(clientMessageVo.getMessage());
        messageVo.setClient(clientMessageVo.getClient());
        messageVo.setUserId(clientMessageVo.getUserId());
        messageVo.setServer(conversationDTO.getServer());
        webSocket.sendMessageToServer(messageVo);
    }
}
