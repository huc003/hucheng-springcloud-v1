package com.example.service;

import com.example.pojo.vo.ClientMessageVo;

import java.io.IOException;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
public interface CustomSService {
    void sendToCs(ClientMessageVo clientMessageVo) throws IOException;
}
