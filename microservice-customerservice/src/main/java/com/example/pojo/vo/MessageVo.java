package com.example.pojo.vo;

import lombok.Data;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
@Data
public class MessageVo {
    private String client;
    private String server;
    private String message;
    private String userId;
}
