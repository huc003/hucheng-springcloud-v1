package com.example.pojo.dto;

import lombok.Data;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
@Data
public class ConversationDTO {
    private String server;
    private String userId;
}
