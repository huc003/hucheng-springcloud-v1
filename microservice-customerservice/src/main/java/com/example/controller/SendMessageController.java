package com.example.controller;

import com.example.pojo.vo.ClientMessageVo;
import com.example.pojo.vo.MessageVo;
import com.example.service.CustomSService;
import com.example.websocket.WebSocketClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
@RestController
public class SendMessageController {

    @Autowired
    private WebSocketClient webSocketClient;

    @Autowired
    private CustomSService customSService;

    /**
     * @Author: hualao
     * @Date:   2018/12/19 13:39
     * @Description: 用户发送消息给客服
    **/
    @RequestMapping("sendToCs")
    public void sendToCs(@RequestBody ClientMessageVo clientMessageVo) throws IOException {
        //分配客服
        customSService.sendToCs(clientMessageVo);
    }

    /**
     * @Author: hualao
     * @Date:   2018/12/19 13:39
     * @Description: 客服发送消息给用户
    **/
    @RequestMapping("sendClient")
    public void sendClient(@RequestBody MessageVo messageVo) throws IOException {
        webSocketClient.sendMessageToClient(messageVo);
    }


}
