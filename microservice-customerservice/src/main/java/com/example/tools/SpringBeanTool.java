package com.example.tools;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
@Component
@Lazy(false)
public class SpringBeanTool extends SpringBaseBeanTool {


}
