package com.example.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
public enum CustomSEnum {
    /**
     * 路由枚举
     */
    CUSTOM_S_LIST("customs:list");

    @Setter
    @Getter
    private String key;

    CustomSEnum(String key) {
        this.key = key;
    }

    public static CustomSEnum get(String key) {
        for (CustomSEnum c : CustomSEnum.values()) {
            if (c.key == key) {
                return c;
            }
        }
        return null;
    }
}
