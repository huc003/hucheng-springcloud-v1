package com.example.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/19
 * @Description: 类描述
 **/
public enum ConversationEnum {
    /**
     * 路由枚举
     */
    CONVERSATION_LIST("conversation:list:");

    @Setter
    @Getter
    private String key;

    ConversationEnum(String key) {
        this.key = key;
    }

    public static ConversationEnum get(String key) {
        for (ConversationEnum c : ConversationEnum.values()) {
            if (c.key == key) {
                return c;
            }
        }
        return null;
    }
}
