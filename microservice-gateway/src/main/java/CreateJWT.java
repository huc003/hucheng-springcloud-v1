import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/5
 * @Description: 类描述
 **/
public class CreateJWT {
    public static void main(String[] args) {
        JwtBuilder builder = Jwts.builder().setId("123").setSubject("jwt所面向到用户")
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256,"admin");
        String s = builder.compact();
        System.out.println(s);

        //解析token
        Claims claims = Jwts.parser().setSigningKey("admin").parseClaimsJws(s).getBody();
        System.out.println("id "+claims.getId());
        System.out.println("subject "+claims.getSubject());
        System.out.println("IssuedAt "+claims.getIssuedAt());

    }
}
