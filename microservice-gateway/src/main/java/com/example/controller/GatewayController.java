package com.example.controller;

import com.example.tools.RouteTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 网关路由控制层
 **/
@Slf4j
@RestController
public class GatewayController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${refresh.url}")
    private String refreshUrl;

    @GetMapping("refresh")
    public void refresh(){
        ThreadPoolExecutor executor = new ThreadPoolExecutor(6, 10, 5, TimeUnit.SECONDS, new SynchronousQueue<>());
        executor.execute(()-> RouteTools.refresh(restTemplate,refreshUrl));
    }

}
