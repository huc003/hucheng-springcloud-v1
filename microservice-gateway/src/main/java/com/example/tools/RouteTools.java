package com.example.tools;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 类描述
 **/
public class RouteTools {

    public static void refresh(RestTemplate restTemplate,String url){
        //刷新路由
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> formEntity = new HttpEntity<>("", headers);
        restTemplate.postForEntity(url+"/actuator/refresh",formEntity,String.class);
    }
}
