package com.example.pojo.vo;

import lombok.Data;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/12/5
 * @Description: 类描述
 **/
@Data
public class UserVo {
    private String userId;
    private String username;
    private String password;
}
