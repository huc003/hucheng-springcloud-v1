package com.example.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 网关路由bean
 **/
@Data
public class RouteVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String path;
    private String group;
    private Integer weight;
    private String uri;

}
