1、项目介绍
microservice-eureka                 注册中心
microservice-gateway                网关
microservice-operation              运维项目
microservice-provider-user          用户
microservice-provider-order         订单
microservice-provider-message       消息
microservice-message-queue          消息队列
microservice-customservice          客服

2、启动顺序
注册中心--》其他的微服务--》网关

3、需要修改的地方
服务中只要有redis的地方都必须修改成自己的
order中的数据库地址修改成自己的

4、项目只是基本的功能案例
