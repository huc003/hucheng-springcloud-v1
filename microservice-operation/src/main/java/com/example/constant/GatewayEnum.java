package com.example.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 网关路由枚举类
 **/
public enum GatewayEnum {
    /**
     * 路由枚举
     */
    ROUTE_LIST("route:list");

    @Setter
    @Getter
    private String key;

    GatewayEnum(String key) {
        this.key = key;
    }

    public static GatewayEnum get(String key) {
        for (GatewayEnum c : GatewayEnum.values()) {
            if (c.key == key) {
                return c;
            }
        }
        return null;
    }
}
