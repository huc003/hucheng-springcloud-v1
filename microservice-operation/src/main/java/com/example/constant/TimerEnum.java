package com.example.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/23
 * @Description: 类描述
 **/
public enum TimerEnum {
    /**
     * 定时器枚举
     */
    TIMER_LIST("timer:list");

    @Setter
    @Getter
    private String key;

    TimerEnum(String key) {
        this.key = key;
    }

    public static TimerEnum get(String key) {
        for (TimerEnum c : TimerEnum.values()) {
            if (c.key == key) {
                return c;
            }
        }
        return null;
    }
}
