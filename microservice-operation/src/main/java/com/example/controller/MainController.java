package com.example.controller;

import com.example.pojo.bean.UserBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 类描述
 **/
@Controller
public class MainController {
    @RequestMapping("main")
    public String login(HttpServletRequest request) {
        //判断用户是否登录
        UserBean userBean = (UserBean) request.getSession().getAttribute("userInfo");
        if(userBean==null){
            return "redirect:/";
        }
        return "main";
    }

    @RequestMapping("top")
    public String top(Model model) {
        model.addAttribute("realName","胡成");
        return "view/top";
    }

    @RequestMapping("left")
    public String left(Model model) {
        return "view/left";
    }

    @RequestMapping("right")
    public String right() {
        return "view/right";
    }

}
