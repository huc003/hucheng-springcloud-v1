package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.component.InitTimer;
import com.example.constant.TimerEnum;
import com.example.pojo.bean.TimerBean;
import com.example.pojo.vo.TimerVo;
import com.example.tools.RedisTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/23
 * @Description: 定时器
 **/
@Slf4j
@Controller
public class TimerController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private InitTimer initTimer;

    @RequestMapping(value = "timer", produces = "text/plain;charset=UTF-8")
    public String timerList(Model model, String message, String color) {
        List<TimerBean> lists = RedisTools.hget(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), TimerBean.class);
        model.addAttribute("timers", lists);
        //发送消息
        model.addAttribute("is_show", 0);
        if (message != null) {
            model.addAttribute("is_show", 1);
            model.addAttribute("color", color);
        }
        model.addAttribute("message", message);
        return "timer/timer";
    }

    @PostMapping("saveTimer")
    public String saveTimer(TimerVo timerVo) throws UnsupportedEncodingException {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        timerVo.setId(uuid);
        RedisTools.hset(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), uuid, JSON.toJSONString(timerVo));
        initTimer.saveScheduler(timerVo);
        return "redirect:timer?message=" + URLEncoder.encode("新增成功", "utf-8") + "&color=009688";
    }

    @RequestMapping("timerById")
    @ResponseBody
    public String timerById(String id) {
        TimerBean timerBean = RedisTools.hget(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), id, TimerBean.class);
        return JSON.toJSONString(timerBean);
    }

    @PostMapping("updateTimer")
    public String updateTimer(TimerVo timerVo) throws UnsupportedEncodingException {
        //判断key是否存在
        if (!RedisTools.existsHash(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), timerVo.getId())) {
            log.info("定时器key不存在直接返回");
            return "redirect:timer?message=" + URLEncoder.encode("定时器key不存在直接返回", "utf-8") + "&color=FF414B";
        }
        RedisTools.hset(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), timerVo.getId(), JSON.toJSONString(timerVo));
        initTimer.init();
        return "redirect:timer?message=" + URLEncoder.encode("修改成功", "utf-8") + "&color=009688";
    }

    @PostMapping("shutdownTimer")
    public String shutdownTimer(String sdId) throws UnsupportedEncodingException {
        //判断key是否存在
        if (!RedisTools.existsHash(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), sdId)) {
            log.info("定时器key不存在直接返回");
            return "redirect:timer?message=" + URLEncoder.encode("定时器key不存在直接返回", "utf-8") + "&color=FF414B";
        }
        TimerBean timerBean = RedisTools.hget(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), sdId, TimerBean.class);
        timerBean.setStatus(timerBean.getStatus() == 1 ? 2 : 1);
        RedisTools.hset(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), sdId, JSON.toJSONString(timerBean));
        initTimer.init();
        if (timerBean.getStatus() == 1) {
            return "redirect:timer?message=" + URLEncoder.encode("开启成功", "utf-8") + "&color=009688";
        }
        return "redirect:timer?message=" + URLEncoder.encode("关闭成功", "utf-8") + "&color=009688";
    }

    @RequestMapping("delTimer")
    public String delTimer(String delId) throws UnsupportedEncodingException {
        long l = RedisTools.hdel(stringRedisTemplate, TimerEnum.TIMER_LIST.getKey(), delId);
        if (l > 0) {
            initTimer.init();
            return "redirect:timer?message=" + URLEncoder.encode("删除成功", "utf-8") + "&color=009688";
        }
        return "redirect:timer?message=" + URLEncoder.encode("删除失败", "utf-8") + "&color=FF414B";
    }

}
