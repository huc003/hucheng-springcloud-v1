package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.constant.GatewayEnum;
import com.example.pojo.vo.RouteVo;
import com.example.tools.RedisTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 类描述
 **/
@Slf4j
@Controller
public class RouteController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${gateway.url}")
    private String refreshUrl;

    @RequestMapping(value = "route", produces = "text/plain;charset=UTF-8")
    public String routeList(Model model, String message, String color) {
        List<RouteVo> lists = RedisTools.hget(stringRedisTemplate, GatewayEnum.ROUTE_LIST.getKey(), RouteVo.class);
        model.addAttribute("routes", lists);
        //发送消息
        model.addAttribute("is_show", 0);
        if (message != null) {
            model.addAttribute("is_show", 1);
            model.addAttribute("color", color);
        }
        model.addAttribute("message", message);
        return "route/route";
    }

    @PostMapping("saveGateway")
    public String saveGateway(RouteVo routeVo) throws UnsupportedEncodingException {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        routeVo.setId(uuid);
        RedisTools.hset(stringRedisTemplate, GatewayEnum.ROUTE_LIST.getKey(), uuid, JSON.toJSONString(routeVo));
        restTemplate.getForObject(refreshUrl+"/refresh", String.class);
        return "redirect:route?message=" + URLEncoder.encode("新增成功", "utf-8") + "&color=009688";
    }

    @PostMapping("updateGateway")
    public String updateGateway(RouteVo routeVo) throws UnsupportedEncodingException {
        //判断key是否存在
        if (!RedisTools.existsHash(stringRedisTemplate, GatewayEnum.ROUTE_LIST.getKey(), routeVo.getId())) {
            log.info("网关路由key不存在直接返回");
            return "redirect:route?message=" + URLEncoder.encode("网关路由key不存在直接返回", "utf-8") + "&color=FF414B";
        }
        RedisTools.hset(stringRedisTemplate, GatewayEnum.ROUTE_LIST.getKey(), routeVo.getId(), JSON.toJSONString(routeVo));
        restTemplate.getForObject(refreshUrl+"/refresh", String.class);
        return "redirect:route?message=" + URLEncoder.encode("修改成功", "utf-8") + "&color=009688";
    }

    @RequestMapping("routeById")
    @ResponseBody
    public String routeById(String id) {
        RouteVo routeVo = RedisTools.hget(stringRedisTemplate, GatewayEnum.ROUTE_LIST.getKey(), id, RouteVo.class);
        return JSON.toJSONString(routeVo);
    }

    @RequestMapping("deleteGateway")
    public String deleteGateway(String delId) throws UnsupportedEncodingException {
        long l = RedisTools.hdel(stringRedisTemplate, GatewayEnum.ROUTE_LIST.getKey(), delId);
        if (l > 0) {
            restTemplate.getForObject(refreshUrl+"/refresh", String.class);
            return "redirect:route?message=" + URLEncoder.encode("删除成功", "utf-8") + "&color=009688";
        }
        return "redirect:route?message=" + URLEncoder.encode("删除失败", "utf-8") + "&color=FF414B";
    }

}
