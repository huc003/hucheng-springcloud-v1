package com.example.controller;

import com.example.pojo.bean.UserBean;
import com.example.pojo.vo.LoginVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 登录
 **/
@Controller
public class LoginController {

    @RequestMapping("/")
    public String loginPage(HttpServletRequest request, Model model) {
        UserBean userBean = (UserBean) request.getSession().getAttribute("userInfo");
        if (userBean != null) {
            return "redirect:/main";
        }
        model.addAttribute("is_show", 0);
        model.addAttribute("color", "");
        model.addAttribute("message", "");
        return "login";
    }

    @RequestMapping("signOut")
    @ResponseBody
    public String signOut(HttpServletRequest request) {
        request.getSession().removeAttribute("userInfo");
        return "ok";
    }

    @RequestMapping("login")
    public String login(LoginVo loginVo, Model model, HttpServletRequest request) {
        if (!loginVo.getUsername().equals("admin") || !loginVo.getPassword().equals("admin")) {
            model.addAttribute("is_show", 1);
            model.addAttribute("color", "FF414B");
            model.addAttribute("message", "用户名或密码错误！");
            return "login";
        }

        UserBean userBean = new UserBean();
        userBean.setUsername(loginVo.getUsername());
        request.getSession().setAttribute("userInfo", userBean);

        return "redirect:main";
    }
}
