package com.example.pojo.vo;

import lombok.Data;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: 类描述
 **/
@Data
public class LoginVo {
    private String username;
    private String password;
    private String isPassword;
}
