package com.example.pojo.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/22
 * @Description: 类描述
 **/
@Data
public class UserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;
}
