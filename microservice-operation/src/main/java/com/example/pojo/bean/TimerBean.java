package com.example.pojo.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/23
 * @Description: 类描述
 **/
@Data
public class TimerBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**id标识*/
    private String id;
    /**定时器名称*/
    private String name;
    /**定时器请求地址*/
    private String uri;
    private String cron;
    private Integer status;
}
