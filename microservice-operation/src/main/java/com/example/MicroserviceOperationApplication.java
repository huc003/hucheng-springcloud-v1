package com.example;

import com.example.component.InitTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceOperationApplication implements CommandLineRunner{

	@Autowired
	private InitTimer initTimer;

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceOperationApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		//初始化定时器
		initTimer.init();
	}
}
