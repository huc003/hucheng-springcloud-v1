//package com.example;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.quartz.*;
//import org.quartz.impl.StdSchedulerFactory;
//import org.quartz.impl.calendar.CronCalendar;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.scheduling.support.CronSequenceGenerator;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.Calendar;
//
//import static org.quartz.JobBuilder.newJob;
//import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
//import static org.quartz.TriggerBuilder.newTrigger;
//
////@RunWith(SpringRunner.class)
////@SpringBootTest
//public class MicroserviceOperationApplicationTests {
//
//    //	@Test
////	public void contextLoads() {
////
////
////	}
//    public static void timer1() {
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, 9); // 控制时
//        calendar.set(Calendar.MINUTE, 24);       // 控制分
//        calendar.set(Calendar.SECOND, 0);       // 控制秒
//
//        Date time = calendar.getTime();         // 得出执行任务的时间,此处为今天的12：00：00
//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            public void run() {
//                System.out.println("-------设定要指定任务--------");
//            }
//        }, time);// 设定指定的时间time,此处为2000毫秒
//    }
//
//    public static void timer4() {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, 9); // 控制时
//        calendar.set(Calendar.MINUTE, 23);       // 控制分
//        calendar.set(Calendar.SECOND, 0);       // 控制秒
//
//        Date time = calendar.getTime();         // 得出执行任务的时间,此处为今天的12：00：00
//
//        Timer timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask() {
//            public void run() {
//                System.out.println("-------设定要指定任务1--------");
//            }
//        }, time, 1000 * 60 * 60 * 24);// 这里设定将延时每天固定执行
//    }
//
//
//    public static void timer3() {
//        Timer timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask() {
//            public void run() {
//                System.out.println("-------设定要指定任务2--------");
//            }
//        }, 1000, 5000);
//    }
//
//    public static void main(String[] args) throws Exception {
////		timer3();
////		timer4();
////		timer1();
////		System.out.println(getDate("5 * * * * ?"));
//
//
//
//    }
//
//    private static final String CRON_DATE_FORMAT = "ss mm HH dd MM ? yyyy";
//
//    /***
//     *
//     * @param cron Quartz cron的类型的日期
//     * @return  Date日期
//     */
//
//    public static Date getDate(final String cron) {
//        if(cron == null) {
//            return null;
//        }
//        SimpleDateFormat sdf = new SimpleDateFormat(CRON_DATE_FORMAT);
//        Date date = null;
//        try {
//            date = sdf.parse(cron);
//        } catch (ParseException e) {
//            return null;// 此处缺少异常处理,自己根据需要添加
//        }
//        return date;
//    }
//
//}
