- 网关路由key
- route:list
- 网关路由数据
{
  "group": "user-group",
  "id": "5d98a14abf68436aa6e8a78cdf210e71",
  "path": "/user/**",
  "uri": "http://192.168.0.111:9091",
  "weight": 51
}


1、安装 showdoc
    
	1. 
# 由于国内镜像与官网原镜像同步可能延迟，如果有网络条件，建议使用原镜像。如果网络不好，建议使用国内镜像
	2. 
# 国内镜像安装命令
	3. 
docker pull registry.docker-cn.com/star7th/showdoc
	4. 
#国外官方镜像安装命令
	5. 
docker pull star7th/showdoc
	6. 
#新建存放showdoc数据的目录
	7. 
mkdir /showdoc_data
	8. 
mkdir /showdoc_data/html
	9. 
chmod 777 -R /showdoc_data
	10. 
#启动showdoc容器。启动完了后别忘记后面还有转移数据的步骤。
	11. 
docker run -d --name showdoc -p 4999:80 -v /showdoc_data/html:/var/www/html/ registry.docker-cn.com/star7th/showdoc
	12. 
#转移数据
	13. 
docker exec showdoc \cp -fr /showdoc_data/html/ /var/www/
	14. 
# 权限
	15. 
chmod 777 -R /showdoc_data


    

2、默认用户名密码
showdoc/123456


websocket
<!DOCTYPE html>
<html>
<head>
    <title>websocket</title>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.min.js"></script>
    <script src="http://cdn.bootcss.com/stomp.js/2.3.3/stomp.min.js"></script>
    <script src="https://cdn.bootcss.com/sockjs-client/1.1.4/sockjs.min.js"></script>
</head>
 
<body>
    <div style="margin: auto;text-align: center">
        <h1>Welcome to websocket</h1>
    </div>
    <br/>
    <div style="margin: auto;text-align: center">
        <select id="onLineUser">
            <option>--所有--</option>
        </select>
        <input id="text" type="text" />
        <button onclick="send()">发送消息</button>
    </div>
    <br>
    <div style="margin-right: 10px;text-align: right">
        <button onclick="closeWebSocket()">关闭连接</button>
    </div>
    <hr/>
    <div id="message" style="text-align: center;"></div>
    <input  type="text" value="lisi" id="username" style="display: none" />
</body>
 
 
<script type="text/javascript">
    var webSocket;
    var commWebSocket;
    if ("WebSocket" in window)
    {
        webSocket = new WebSocket("ws://localhost:8767/websocket/"+document.getElementById('username').value);
 
        //连通之后的回调事件
        webSocket.onopen = function()
        {
            //webSocket.send( document.getElementById('username').value+"已经上线了");
            console.log("已经连通了websocket");
            setMessageInnerHTML("已经连通了websocket");
        };
 
        //接收后台服务端的消息
        webSocket.onmessage = function (evt)
        {
            var received_msg = evt.data;
            console.log("数据已接收:" +received_msg);
            var obj = JSON.parse(received_msg);
            
            setMessageInnerHTML(obj.user_id+"对"+obj.user_id+"说："+obj.msg);
        };
 
        //连接关闭的回调事件
        webSocket.onclose = function()
        {
            console.log("连接已关闭...");
            setMessageInnerHTML("连接已经关闭....");
        };
    }
    else{
        // 浏览器不支持 WebSocket
        alert("您的浏览器不支持 WebSocket!");
    }
    //将消息显示在网页上
    function setMessageInnerHTML(innerHTML) {
        document.getElementById('message').innerHTML += innerHTML + '<br/>';
    }
 
    function closeWebSocket() {
        //直接关闭websocket的连接
        webSocket.close();
    }
 
    // function send() {
    //     var selectText = $("#onLineUser").find("option:selected").text();
    //     if(selectText=="--所有--"){
    //         selectText = "All";
    //     }
    //     else{
    //         setMessageInnerHTML(document.getElementById('username').value+"对"+selectText+"说："+ $("#text").val());
    //     }
    //     var message = {
    //         "message":document.getElementById('text').value,
    //         "username":document.getElementById('username').value,
    //         "to":selectText
    //     };
    //     webSocket.send(JSON.stringify(message));
    //     $("#text").val("");
 
    // }
</script>
 
</html>