package com.example.controller;

import com.example.service.RefreshRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/7/23
 * @Description: 刷新路由ctrl
 **/
@RequestMapping("refresh")
@RestController
public class RefreshRouteCtrl {

    @Autowired
    private RefreshRouteService refreshRouteService;

    @GetMapping("route")
    public String refreshRoute() {
        refreshRouteService.refreshRoute();
        return "success";
    }
}
