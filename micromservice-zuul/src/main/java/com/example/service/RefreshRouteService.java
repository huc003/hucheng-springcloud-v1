package com.example.service;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/7/23
 * @Description: 刷新路由
 **/
public interface RefreshRouteService {
    /**
     * @Author: hualao
     * @Date: 2018/7/23 16:35
     * @Description: 刷新路由
     **/
    void refreshRoute();
}
