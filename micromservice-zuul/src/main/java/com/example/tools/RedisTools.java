package com.example.tools;

import com.alibaba.fastjson.JSON;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/20
 * @Description: redis工具类
 **/
public class RedisTools {

    public static <T> List<T> hget(StringRedisTemplate stringRedisTemplate,String key,Class<T> clazz){
        List<Object> list = stringRedisTemplate.opsForHash().values(key);
        return JSON.parseArray(list.toString(), clazz);
    }

    public static void hset(StringRedisTemplate stringRedisTemplate,String h,String key,String value){
        stringRedisTemplate.opsForHash().put(h,key,value);
    }

    public static Boolean existsHash(StringRedisTemplate stringRedisTemplate,String h,String key){
        return stringRedisTemplate.opsForHash().hasKey(h,key);
    }

}
