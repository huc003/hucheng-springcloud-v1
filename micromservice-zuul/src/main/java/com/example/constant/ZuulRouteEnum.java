package com.example.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/7/23
 * @Description: 路由限流枚举
 **/
public enum ZuulRouteEnum {

    /**
     * 路由枚举
     */
    ZUUL_ROUTE_LIST("zuul:route:list");

    @Getter
    @Setter
    private String key;

    ZuulRouteEnum(String key) {
        this.key = key;
    }

    public static ZuulRouteEnum get(String key) {
        for (ZuulRouteEnum c : ZuulRouteEnum.values()) {
            if (c.key == key) {
                return c;
            }
        }
        return null;
    }
}
