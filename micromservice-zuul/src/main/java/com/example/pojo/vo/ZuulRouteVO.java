package com.example.pojo.vo;

import lombok.Data;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/7/23
 * @Description: 路由参数
 **/
@Data
public class ZuulRouteVO {
    /**
     * id主键
     */
    private String id;

    /**
     * 网关路由访问地址
     */
    private String path;

    /**
     * @Author: hualao
     * @Date:   2018/9/3 16:09
     * @Description: 服务地址
    **/
    private String url;

    /**
     * 服务id
     */
    private String serviceId;

    /**
     * 是否启用 true启用 false不启用
     */
    private Boolean enabled;

}
