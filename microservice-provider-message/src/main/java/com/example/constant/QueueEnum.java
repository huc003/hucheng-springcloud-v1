package com.example.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/29
 * @Description: 枚举类
 **/
public enum QueueEnum {
    /**
     * 路由枚举
     */
    MESSAGE_QUEUE("message:list");

    @Setter
    @Getter
    private String key;

    QueueEnum(String key) {
        this.key = key;
    }

    public static QueueEnum get(String key) {
        for (QueueEnum c : QueueEnum.values()) {
            if (c.key == key) {
                return c;
            }
        }
        return null;
    }
}
