package com.example;

import com.example.queue.QueueDBInit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class MicroserviceProviderMessageApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceProviderMessageApplication.class, args);
	}

	@Autowired
	private QueueDBInit dbInit;

	@Override
	public void run(String... strings) throws Exception {
		//从数据库读取前20秒未发送的短信放入到队列中
		dbInit.init();
	}
}
