package com.example.tools;

import com.alibaba.fastjson.JSON;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/29
 * @Description: 类描述
 **/
public class RedisTools {

    public static <T> T lPop(StringRedisTemplate stringRedisTemplate, String key, Class<T> clazz) {
        String str = stringRedisTemplate.opsForList().leftPop(key);
        return JSON.parseObject(str, clazz);
    }

    public static <T> T rPop(StringRedisTemplate stringRedisTemplate, String key, Class<T> clazz) {
        String str = stringRedisTemplate.opsForList().rightPop(key);
        return JSON.parseObject(str, clazz);
    }

    public static Long lpush(StringRedisTemplate stringRedisTemplate, String key, String val) {
        return stringRedisTemplate.opsForList().leftPush(key, val);
    }

    public static Long rpush(StringRedisTemplate stringRedisTemplate, String key, String val) {
        return stringRedisTemplate.opsForList().rightPush(key, val);
    }

}
