package com.example.queue;

import org.springframework.stereotype.Component;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/29
 * @Description: 读取前10秒的数据库未发送的短信数据放入队列中
 **/
@Component
public class QueueDBInit {

    public void init() {
        while (true) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }

}
