package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.constant.QueueEnum;
import com.example.pojo.vo.MessageBean;
import com.example.tools.RedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/29
 * @Description: 添加短信内容
 **/
@RestController
public class MessageController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("saveMessage")
    public String saveMessage(@RequestBody MessageBean messageBean) {
        //添加短信到消息队列
        Long l = RedisTools.lpush(stringRedisTemplate, QueueEnum.MESSAGE_QUEUE.getKey(), JSON.toJSONString(messageBean));
        if (l > 0) {
            //添加短信到数据库

            return "success";
        }
        return "fail";
    }

}
