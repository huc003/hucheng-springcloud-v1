package com.example.pojo;

import lombok.Data;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/29
 * @Description: 类描述
 **/
@Data
public class MessageBean {
    private Integer id;
    private String phone;
    private String context;
}
