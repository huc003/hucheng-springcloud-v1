package com.example.tools;

import com.alibaba.fastjson.JSON;
import com.example.pojo.MessageBean;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/29
 * @Description: 发送消息工具类
 **/
@Slf4j
public class MessageTools {
    public static void send(MessageBean messageBean) {
        log.info("发送短信消息 --> {}", JSON.toJSON(messageBean));
    }
}
