package com.example;

import com.example.queue.QueueInit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class MicroserviceMessageQueueApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceMessageQueueApplication.class, args);
	}

	@Autowired
	private QueueInit init;

	@Override
	public void run(String... strings) throws Exception {
		//启动消息队列
		init.init();
	}
}
