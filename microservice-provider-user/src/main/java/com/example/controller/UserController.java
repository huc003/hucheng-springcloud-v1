package com.example.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/19
 * @Description: 类描述
 **/
@RestController
public class UserController {

    @PostMapping("userInfo")
    public String userInfo() {
        System.out.println("测试定时器请求");
        return "{userInfo:zhangsan}";
    }
}
