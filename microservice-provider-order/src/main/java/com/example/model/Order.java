package com.example.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order implements Serializable {
    private String orderId;

    private String memberId;

    private Integer erpState;

    private Integer sourceType;

    private String userId;

    private String userName;

    private BigDecimal originalPrice;

    private BigDecimal commissionPrice;

    private String expressName;

    private String expressNo;

    private BigDecimal express;

    private Integer payType;

    private Date payTime;

    private Integer payState;

    private BigDecimal discountPrice;

    private BigDecimal couponPrice;

    private BigDecimal sharePrice;

    private BigDecimal payPrice;

    private String payNo;

    private String name;

    private String iphone;

    private Integer chinaId;

    private String address;

    private Integer balanceState;

    private Integer commentState;

    private Boolean refundStatus;

    private Integer orderState;

    private Date confirmTime;

    private Date closeTime;

    private Date commentTime;

    private Date orderTime;

    private Date takeTime;

    private Date balanceTime;

    private Date sendTime;

    private Date createTime;

    private Date updateTime;

    private Integer isDel;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Order other = (Order) that;
        return (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getMemberId() == null ? other.getMemberId() == null : this.getMemberId().equals(other.getMemberId()))
            && (this.getErpState() == null ? other.getErpState() == null : this.getErpState().equals(other.getErpState()))
            && (this.getSourceType() == null ? other.getSourceType() == null : this.getSourceType().equals(other.getSourceType()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getOriginalPrice() == null ? other.getOriginalPrice() == null : this.getOriginalPrice().equals(other.getOriginalPrice()))
            && (this.getCommissionPrice() == null ? other.getCommissionPrice() == null : this.getCommissionPrice().equals(other.getCommissionPrice()))
            && (this.getExpressName() == null ? other.getExpressName() == null : this.getExpressName().equals(other.getExpressName()))
            && (this.getExpressNo() == null ? other.getExpressNo() == null : this.getExpressNo().equals(other.getExpressNo()))
            && (this.getExpress() == null ? other.getExpress() == null : this.getExpress().equals(other.getExpress()))
            && (this.getPayType() == null ? other.getPayType() == null : this.getPayType().equals(other.getPayType()))
            && (this.getPayTime() == null ? other.getPayTime() == null : this.getPayTime().equals(other.getPayTime()))
            && (this.getPayState() == null ? other.getPayState() == null : this.getPayState().equals(other.getPayState()))
            && (this.getDiscountPrice() == null ? other.getDiscountPrice() == null : this.getDiscountPrice().equals(other.getDiscountPrice()))
            && (this.getCouponPrice() == null ? other.getCouponPrice() == null : this.getCouponPrice().equals(other.getCouponPrice()))
            && (this.getSharePrice() == null ? other.getSharePrice() == null : this.getSharePrice().equals(other.getSharePrice()))
            && (this.getPayPrice() == null ? other.getPayPrice() == null : this.getPayPrice().equals(other.getPayPrice()))
            && (this.getPayNo() == null ? other.getPayNo() == null : this.getPayNo().equals(other.getPayNo()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getIphone() == null ? other.getIphone() == null : this.getIphone().equals(other.getIphone()))
            && (this.getChinaId() == null ? other.getChinaId() == null : this.getChinaId().equals(other.getChinaId()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getBalanceState() == null ? other.getBalanceState() == null : this.getBalanceState().equals(other.getBalanceState()))
            && (this.getCommentState() == null ? other.getCommentState() == null : this.getCommentState().equals(other.getCommentState()))
            && (this.getRefundStatus() == null ? other.getRefundStatus() == null : this.getRefundStatus().equals(other.getRefundStatus()))
            && (this.getOrderState() == null ? other.getOrderState() == null : this.getOrderState().equals(other.getOrderState()))
            && (this.getConfirmTime() == null ? other.getConfirmTime() == null : this.getConfirmTime().equals(other.getConfirmTime()))
            && (this.getCloseTime() == null ? other.getCloseTime() == null : this.getCloseTime().equals(other.getCloseTime()))
            && (this.getCommentTime() == null ? other.getCommentTime() == null : this.getCommentTime().equals(other.getCommentTime()))
            && (this.getOrderTime() == null ? other.getOrderTime() == null : this.getOrderTime().equals(other.getOrderTime()))
            && (this.getTakeTime() == null ? other.getTakeTime() == null : this.getTakeTime().equals(other.getTakeTime()))
            && (this.getBalanceTime() == null ? other.getBalanceTime() == null : this.getBalanceTime().equals(other.getBalanceTime()))
            && (this.getSendTime() == null ? other.getSendTime() == null : this.getSendTime().equals(other.getSendTime()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getIsDel() == null ? other.getIsDel() == null : this.getIsDel().equals(other.getIsDel()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getMemberId() == null) ? 0 : getMemberId().hashCode());
        result = prime * result + ((getErpState() == null) ? 0 : getErpState().hashCode());
        result = prime * result + ((getSourceType() == null) ? 0 : getSourceType().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getOriginalPrice() == null) ? 0 : getOriginalPrice().hashCode());
        result = prime * result + ((getCommissionPrice() == null) ? 0 : getCommissionPrice().hashCode());
        result = prime * result + ((getExpressName() == null) ? 0 : getExpressName().hashCode());
        result = prime * result + ((getExpressNo() == null) ? 0 : getExpressNo().hashCode());
        result = prime * result + ((getExpress() == null) ? 0 : getExpress().hashCode());
        result = prime * result + ((getPayType() == null) ? 0 : getPayType().hashCode());
        result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
        result = prime * result + ((getPayState() == null) ? 0 : getPayState().hashCode());
        result = prime * result + ((getDiscountPrice() == null) ? 0 : getDiscountPrice().hashCode());
        result = prime * result + ((getCouponPrice() == null) ? 0 : getCouponPrice().hashCode());
        result = prime * result + ((getSharePrice() == null) ? 0 : getSharePrice().hashCode());
        result = prime * result + ((getPayPrice() == null) ? 0 : getPayPrice().hashCode());
        result = prime * result + ((getPayNo() == null) ? 0 : getPayNo().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getIphone() == null) ? 0 : getIphone().hashCode());
        result = prime * result + ((getChinaId() == null) ? 0 : getChinaId().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getBalanceState() == null) ? 0 : getBalanceState().hashCode());
        result = prime * result + ((getCommentState() == null) ? 0 : getCommentState().hashCode());
        result = prime * result + ((getRefundStatus() == null) ? 0 : getRefundStatus().hashCode());
        result = prime * result + ((getOrderState() == null) ? 0 : getOrderState().hashCode());
        result = prime * result + ((getConfirmTime() == null) ? 0 : getConfirmTime().hashCode());
        result = prime * result + ((getCloseTime() == null) ? 0 : getCloseTime().hashCode());
        result = prime * result + ((getCommentTime() == null) ? 0 : getCommentTime().hashCode());
        result = prime * result + ((getOrderTime() == null) ? 0 : getOrderTime().hashCode());
        result = prime * result + ((getTakeTime() == null) ? 0 : getTakeTime().hashCode());
        result = prime * result + ((getBalanceTime() == null) ? 0 : getBalanceTime().hashCode());
        result = prime * result + ((getSendTime() == null) ? 0 : getSendTime().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getIsDel() == null) ? 0 : getIsDel().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderId=").append(orderId);
        sb.append(", memberId=").append(memberId);
        sb.append(", erpState=").append(erpState);
        sb.append(", sourceType=").append(sourceType);
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", originalPrice=").append(originalPrice);
        sb.append(", commissionPrice=").append(commissionPrice);
        sb.append(", expressName=").append(expressName);
        sb.append(", expressNo=").append(expressNo);
        sb.append(", express=").append(express);
        sb.append(", payType=").append(payType);
        sb.append(", payTime=").append(payTime);
        sb.append(", payState=").append(payState);
        sb.append(", discountPrice=").append(discountPrice);
        sb.append(", couponPrice=").append(couponPrice);
        sb.append(", sharePrice=").append(sharePrice);
        sb.append(", payPrice=").append(payPrice);
        sb.append(", payNo=").append(payNo);
        sb.append(", name=").append(name);
        sb.append(", iphone=").append(iphone);
        sb.append(", chinaId=").append(chinaId);
        sb.append(", address=").append(address);
        sb.append(", balanceState=").append(balanceState);
        sb.append(", commentState=").append(commentState);
        sb.append(", refundStatus=").append(refundStatus);
        sb.append(", orderState=").append(orderState);
        sb.append(", confirmTime=").append(confirmTime);
        sb.append(", closeTime=").append(closeTime);
        sb.append(", commentTime=").append(commentTime);
        sb.append(", orderTime=").append(orderTime);
        sb.append(", takeTime=").append(takeTime);
        sb.append(", balanceTime=").append(balanceTime);
        sb.append(", sendTime=").append(sendTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isDel=").append(isDel);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}