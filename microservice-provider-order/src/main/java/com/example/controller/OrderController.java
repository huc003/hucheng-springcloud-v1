package com.example.controller;

import com.example.pojo.vo.OrderVo;
import com.example.service.OrderService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/19
 * @Description: 类描述
 **/
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("userInfo")
    public PageInfo orderInfo(@RequestBody OrderVo orderVo){
        PageInfo lists = orderService.selectAll(orderVo);
        return lists;
    }

}
