package com.example.pojo.vo;

import com.example.tools.AbstractPageForm;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/27
 * @Description: 类描述
 **/
@Data
public class OrderVo extends AbstractPageForm<OrderVo> implements Serializable {
    private static final long serialVersionUID = 1L;


}
