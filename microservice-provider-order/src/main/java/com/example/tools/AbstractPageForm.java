package com.example.tools;

import com.github.pagehelper.PageHelper;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/27
 * @Description: 类描述
 **/
@Data
public class AbstractPageForm<T extends AbstractPageForm<T>> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected int pageNum = 1;

    protected int pageSize = 10;

    @SuppressWarnings("unchecked")
    public final T startPage() {
        PageHelper.startPage(pageNum, pageSize);
        return (T) this;
    }

}
