package com.example.service.impl;

import com.example.dao.OrderMapper;
import com.example.model.Order;
import com.example.pojo.vo.OrderVo;
import com.example.service.OrderService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/27
 * @Description: 类描述
 **/
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Order selectByPrimaryKey(String orderId) {
        return orderMapper.selectByPrimaryKey(orderId);
    }

    @Override
    public PageInfo selectAll(OrderVo orderVo) {
        List<Order> lists = orderMapper.selectAll(orderVo.startPage());
        PageInfo<Order> pageInfo = new PageInfo<>(lists);
        return pageInfo;
    }
}
