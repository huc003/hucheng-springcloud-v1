package com.example.service;

import com.example.model.Order;
import com.example.pojo.vo.OrderVo;
import com.github.pagehelper.PageInfo;

/**
 * @Author: hualao
 * @Version: 0.0.1V
 * @Date: 2018/11/27
 * @Description: 类描述
 **/
public interface OrderService {

    Order selectByPrimaryKey(String orderId);

    PageInfo selectAll(OrderVo orderVo);

}
