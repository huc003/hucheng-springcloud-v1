package com.example.dao;

import com.example.model.OrderItem;

public interface OrderItemMapper {
    int deleteByPrimaryKey(String childOrderId);

    int insert(OrderItem record);

    int insertSelective(OrderItem record);

    OrderItem selectByPrimaryKey(String childOrderId);

    int updateByPrimaryKeySelective(OrderItem record);

    int updateByPrimaryKey(OrderItem record);
}